import urllib2
import re
import json

from model.CardModel import *
from patterns import *



# Base URLs
SIMPLE_SEARCH_BASE_URL = "http://gatherer.wizards.com/Pages/Search/Default.aspx?"


def parse_multiverse_ids(html):
    return re.compile(SEARCH_MULTIVERSE_ID_PATTERN, re.DOTALL).findall(html)


def parse_cards_titles(html):
    return re.compile(SEARCH_TITLE_PATTERN, re.DOTALL).findall(html)


def parse_mana_costs(html):
    mana_costs = []
    temp_matches = re.compile(SEARCH_MANA_COST_PATTERN, re.DOTALL).findall(html)
    for match in temp_matches:
        symbols_matches = re.compile(SEARCH_MANA_COST_SYMBOLS_PATTERN, re.DOTALL).findall(match)
        mana_cost_string = ""
        for symbol_match in symbols_matches:
            mana_cost_string += str(symbol_match)
        mana_costs.append(mana_cost_string)
    return mana_costs


def parse_converted_mana_costs(mana_costs):
    converted_mana_costs = []
    for cost in mana_costs:
        converted_mana_cost = 0
        for costCharacter in cost:
            if costCharacter.isdigit():
                converted_mana_cost += int(costCharacter)
            else:
                converted_mana_cost += 1
        converted_mana_costs.append(converted_mana_cost)
    return converted_mana_costs


def parse_expansion_texts(html):
    return re.compile(SEARCH_EXPANSION_TEXT_PATTERN, re.DOTALL).findall(html)


def parse_expansion_images_urls(html):
    expansion_images_urls = []
    parse_result = re.compile(SEARCH_EXPANSION_IMAGE_URL_PATTERN, re.DOTALL).findall(html)
    for result in parse_result:
        expansion_images_urls.append("http://gatherer.wizards.com/" + result)
    return expansion_images_urls


def parse_primary_types(html):
    primary_types = []
    primary_types_lines = re.compile(SEARCH_TYPE_LINE_PATTERN, re.DOTALL).findall(html)
    for line in primary_types_lines:
        card_type = line.split('\xe2\x80\x94')
        primary_types.append(card_type[0].strip())

    return primary_types


def partes_subtypes(html):
    subtypes = []
    subtypes_lines = re.compile(SEARCH_TYPE_LINE_PATTERN, re.DOTALL).findall(html)
    for line in subtypes_lines:
        temp_split = line.split('\xe2\x80\x94')
        if len(temp_split) == 2:
            subtype = temp_split[1].split('(')[0]
            subtypes.append(subtype.strip())
        else:
            subtypes.append("")

    return subtypes


def parse_rules(html):
    rules = []
    rules_texts = re.compile(SEARCH_RULES_TEXT_PATTERN, re.DOTALL).findall(html)
    for rule_text in rules_texts:
        rule_text = re.sub(SEARCH_RULES_IMAGES_URL_REPLACEMENT_PATTERN, 'src=\"http://gatherer.wizards.com/Handlers',
                           rule_text)
        rule_text = re.sub('size=small', 'size=medium', rule_text)

        rules.append(rule_text)
    print(len(rules))
    return rules


def parse_simple_search(simple_search_query_string):
    url = SIMPLE_SEARCH_BASE_URL + simple_search_query_string
    # print url
    req = urllib2.Request(url)
    req.add_header('Accept-Language', 'es')
    html = urllib2.urlopen(req).read()

    # Multiverse IDs
    multiverse_ids = parse_multiverse_ids(html)

    # Titles
    titles = parse_cards_titles(html)
    cards_num = len(titles)

    # Mana costs
    mana_costs = parse_mana_costs(html)

    # Converted mana costs
    converted_mana_costs = parse_converted_mana_costs(mana_costs)

    # Expansions texts
    expansion_texts = parse_expansion_texts(html)

    # Expansions images urls
    expansion_images_urls = parse_expansion_images_urls(html)

    # Primary types
    primary_types = parse_primary_types(html)

    # Subtypes
    subtypes = partes_subtypes(html)

    # Rules
    rules = parse_rules(html)

    # Build cards
    cards = []
    for i in range(0, cards_num, 1):
        card_basic_metadata = BasicMetadata(multiverse_ids[i], titles[i], mana_costs[i], converted_mana_costs[i],
                                            primary_types[i], subtypes[i], rules[i], "power", "toughness",
                                            expansion_texts[i], expansion_images_urls[i])
        cards.append(Card(card_basic_metadata))

    # Build response
    search_result = []
    for card in cards:
        search_result.append(card.to_json())

    print(json.dumps(search_result, sort_keys=True, indent=4, separators=(',', ': ')))
    return json.dumps(search_result, sort_keys=True, indent=4, separators=(',', ': '))