class SimpleSearchFields():
    def __init__(self):
        pass

    Color = "color"
    Format = "format"
    Name = "name"
    Set = "set"
    Subtype = "subtype"
    Text = "text"
    Type = "type"


class SimpleSearchBuilder:
    plain_text_string = str
    query_string = str

    def __init__(self):
        self.plain_text_string = ""
        self.query_string = ""

    # colors_matched_exactly parameter only applies for color field
    def add_field(self, field, values, colors_matched_exactly):
        if not (self.query_string.__contains__(field) or self.plain_text_string.__contains__(field)):
            if len(self.query_string) != 0:
                self.query_string += "&"

            if field == SimpleSearchFields.Name or field == SimpleSearchFields.Type or field == SimpleSearchFields.Subtype or field == SimpleSearchFields.Text:
                self.append_field_to_plain_text_string(field, values)
            else:
                self.append_field_to_search_query_string(field, values, colors_matched_exactly)

    def append_field_to_plain_text_string(self, field, values):
        if len(self.plain_text_string) == 0:
            self.plain_text_string = field + "="
        else:
            self.plain_text_string += "||" + field + "="
        for value in values:
            self.plain_text_string += "+[" + value + "]"

    def append_field_to_search_query_string(self, field, values, colors_matched_exactly):
        self.query_string += field + "="
        if field == SimpleSearchFields.Color:
            for value in values:
                if colors_matched_exactly:
                    separator = "+"
                else:
                    separator = "|"
                self.query_string += separator + "[" + value + "]"

        elif field == SimpleSearchFields.Format or field == SimpleSearchFields.Set:
            self.query_string += "["
            for value in values:
                if value == values[0]:
                    self.query_string += "\"" + value + "\""
                else:
                    self.query_string += ", \"" + value + "\""
            self.query_string += "]"

    def get_simple_search_query_string(self):
        if len(self.query_string) != 0:
            return self.query_string + "&" + self.plain_text_string
        else:
            return self.plain_text_string
