# coding=utf-8
from array import *


class ExtendedMetadata:
    flavor_text = str
    rarity = str

    def __init__(self, flavor_text, rarity):
        self.flavor_text = flavor_text
        self.rarity = rarity

    def to_json(self):
        return {
            "flavor_text": self.flavor_text,
            'rarity': self.rarity
        }


class BasicMetadata:
    multiverse_id = str
    name = str
    mana_cost = array('c')
    converted_mana_cost = int
    primary_type = str
    subtype = str
    rules = str
    power = str
    toughness = str
    expansion_text = str
    expansion_image_url = str

    def __init__(self, multiverse_id, name, mana_cost, converted_mana_cost, primary_type, subtype, rules,
                 power, toughness, expansion_text, expansion_image_url):
        self.multiverse_id = multiverse_id
        self.name = name
        self.mana_cost = mana_cost
        self.converted_mana_cost = converted_mana_cost
        self.primary_type = primary_type
        self.subtype = subtype
        self.rules = rules
        self.power = power
        self.toughness = toughness
        self.expansion_text = expansion_text
        self.expansion_image_url = expansion_image_url

    def to_json(self):
        return {
            "multiverse_id": self.multiverse_id,
            'name': self.name,
            'mana_cost': self.mana_cost,
            'converted_mana_cost': self.converted_mana_cost,
            'primary_type': self.primary_type,
            'subtype': self.subtype,
            'rules': self.rules,
            'power': self.power,
            'toughness': self.toughness,
            'expansion_text': self.expansion_text,
            'expansion_image_url': self.expansion_image_url
        }


class Card:
    basic_metadata = BasicMetadata
    extended_metadata = ExtendedMetadata

    def __init__(self, basic_metadata):
        self.basic_metadata = basic_metadata
        self.extended_metadata = ExtendedMetadata("flavor_text", "rarity")

    def to_json(self):
        return {
            'basicMetadata': self.basic_metadata.to_json(),
            'extendedMetadata': self.extended_metadata.to_json()
        }

# BASE_URL = "http://gatherer.wizards.com/Pages/Search/Default.aspx?"
# CARD_TITLE_PATTERN = "ctl00_ctl00_ctl00_MainContent_SubContent_SubContent_ctl00_listRepeater_ctl(.*?)_cardTitle"
# CARD_TITLE_PATTERN = "listRepeater_ctl(.*?)_cardTitle"


# class CardType(Enum):
# Artifact = "Artifact"
# Basic = "Basic"
# Conspiracy = "Conspiracy"
#     Creature = "Creature"
#     Enchantment = "Enchantment"
#     Instant = "Instant"
#     Land = "Land"
#     Legendary = "Legendary"
#     Ongoing = "Ongoing"
#     Phenomenon = "Phenomenon"
#     Plane = "Plane"
#     Planeswalker = "Planeswalker"
#     Scheme = "Scheme"
#     Snow = "Snow"
#     Sorcery = "Sorcery"
#     Tribal = "Tribal"
#     Vanguard = "Vanguard"
#     World = "World"
#
#
# class CardSet(Enum):
#     AlaraReborn = "Alara Reborn"
#     Alliances = "Alliances"
#     Antiquities = "Antiquities"
#     Apocalypse = "Apocalypse"
#     ArabianNights = "Arabian Nights"
#     Archenemy = "Archenemy"
#     AvacynRestored = "Avacyn Restored"
#     BattleRoyaleBoxSet = "Battle Royale Box Set"
#     BeatdownBoxSet = "Beatdown Box Set"
#     BetrayersOfKamigawa = "Betrayers of Kamigawa"
#     BornOfTheGods = "Born of the Gods"
#     ChampionsOfKamigawa = "Champions of Kamigawa"
#     Chronicles = "Chronicles"
#     ClassicSixthEdition = "Classic Sixth Edition"
#     Coldsnap = "Coldsnap"
#     Commander2013Edition = "Commander 2013 Edition"
#     Commander2014 = "Commander 2014"
#     CommandersArsenal = "Commander's Arsenal"
#     Conflux = "Conflux"
#     DarkAscension = "Dark Ascension"
#     Darksteel = "Darksteel"
#     Dissension = "Dissension"
#     DragonsMaze = "Dragon's Maze"
#     DragonsOfTarkir = "Dragons of Tarkir"
#     DDA_DivineVSDemonic = "Duel Decks Anthology, Divine vs. Demonic"
#     DDA_ElvesVSGoblins = "Duel Decks Anthology, Elves vs. Goblins"
#     DDA_GarrukVSLiliana = "Duel Decks Anthology, Garruk vs. Liliana"
#     DDA_JaceVSChandra = "Duel Decks Anthology, Jace vs. Chandra"
#     DD_AjaniVSNicolBolas = "Duel Decks: Ajani vs. Nicol Bolas"
#     DD_DivineVSDemonic = "Duel Decks: Divine vs. Demonic"
#     DD_ElspethVSKiora = "Duel Decks: Elspeth vs. Kiora"
#     DD_ElspethVSTezzeret = "Duel Decks: Elspeth vs. Tezzeret"
#     DD_ElvesVSGoblins = "Duel Decks: Elves vs. Goblins"
#     DD_GarrukVSLiliana = "Duel Decks: Garruk vs. Liliana"
#     DD_HeroesVSMonsters = "Duel Decks: Heroes vs. Monsters"
#     DD_IzzetVSGolgari = "Duel Decks: Izzet vs. Golgari"
#     DD_JaceVSChandra = "Duel Decks: Jace vs. Chandra"
#     DD_JaceVSVraska = "Duel Decks: Jace vs. Vraska"
#     DD_KnightsVSDragons = "Duel Decks: Knights vs. Dragons"
#     DD_PhyrexiaVSTheCoalition = "Duel Decks: Phyrexia vs. the Coalition"
#     DD_SorinVSTibalt = "Duel Decks: Sorin vs. Tibalt"
#     DD_SpeedVSCunning = "Duel Decks: Speed vs. Cunning"
#     DD_VenserVSKoth = "Duel Decks: Venser vs. Koth"
#     EighthEdition = "Eighth Edition"
#     Eventide = "Eventide"
#     Exodus = "Exodus"
#     FallenEmpires = "Fallen Empires"
#     FateReforged = "Fate Reforged"
#     FifthDawn = "Fifth Dawn"
#     FifthEdition = "Fifth Edition"
#     FourthEdition = "Fourth Edition"
#     FTV_Annihilation2014 = "From the Vault: Annihilation (2014)"
#     FTV_Dragons = "From the Vault: Dragons"
#     FTV_Exiled = "From the Vault: Exiled"
#     FTV_Legends = "From the Vault: Legends"
#     FTV_Realms = "From the Vault: Realms"
#     FTV_Relics = "From the Vault: Relics"
#     FTV_Twenty = "From the Vault: Twenty"
#     FutureSight = "Future Sight"
#     Gatecrash = "Gatecrash"
#     Guildpact = "Guildpact"
#     Homelands = "Homelands"
#     IceAge = "Ice Age"
#     Innistrad = "Innistrad"
#     Invasion = "Invasion"
#     JourneyIntoNyx = "Journey into Nyx"
#     Judgment = "Judgment"
#     KhansOfTarkir = "Khans of Tarkir"
#     Legends = "Legends"
#     Legions = "Legions"
#     LimitedEditionAlpha = "Limited Edition Alpha"
#     LimitedEditionBeta = "Limited Edition Beta"
#     Lorwyn = "Lorwyn"
#     Magic2010 = "Magic 2010"
#     Magic2011 = "Magic 2011"
#     Magic2012 = "Magic 2012"
#     Magic2013 = "Magic 2013"
#     Magic2014CoreSet = "Magic 2014 Core Set"
#     Magic2015CoreSet = "Magic 2015 Core Set"
#     MagicOrigins = "Magic Origins"
#     MTG_Commander = "Magic: The Gathering-Commander"
#     MTG_Conspiracy = "Magic: The Gathering—Conspiracy"
