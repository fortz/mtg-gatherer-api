import urllib2
import re
import json

from model.CardModel import *
from patterns import *

CARD_DETAIL_BASE_URL = "http://gatherer.wizards.com/Pages/Card/Details.aspx?multiverseid="


def parse_card_detail(multiverse_id):
    req = urllib2.Request(CARD_DETAIL_BASE_URL + multiverse_id)
    req.add_header('Accept-Language', 'es')
    html = urllib2.urlopen(req).read()
    # html = urllib2.urlopen("file:///Users/albertoguerreromartin/Desktop/Alberto/Development/MTG-Gatherer/card_detail.html").read()
    card_info = {
        'name': re.compile(CARD_NAME_PATTERN, re.DOTALL).findall(html)[0].strip(),
        'converted_mana_cost': re.compile(CARD_CONVERTED_MANA_COST_PATTERN, re.DOTALL).findall(html)[0].strip(),
        'types': re.compile(CARD_TYPES_PATTERN, re.DOTALL).findall(html)[0].strip(),
        'text': re.compile(CARD_TEXT_PATTERN, re.DOTALL).findall(html)[0].strip(),
        'flavor_text': re.compile(CARD_FLAVOR_TEXT_PATTERN, re.DOTALL).findall(html)[0].strip()
    }
    return card_info
