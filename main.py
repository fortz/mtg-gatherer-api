from flask import *

import parser_simple_search
import parse_card_detail
from builders import SimpleSearchBuilder, SimpleSearchFields

app = Flask(__name__)


@app.errorhandler(404)
def not_found():
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.route('/')
def hello_world():
    # card = CardModel.Card("id", "Akroma", "Conspiracy")
    builder = SimpleSearchBuilder()
    builder.add_field(SimpleSearchFields.Text, ["Texto de prueba"], False)
    builder.add_field(SimpleSearchFields.Text, ["Texto de prueba"], False)
    builder.add_field(SimpleSearchFields.Text, ["Texto de prueba"], False)
    builder.add_field(SimpleSearchFields.Type, ["Tipo de prueba"], False)
    builder.add_field(SimpleSearchFields.Color, ["W", "G", "R"], True)
    builder.add_field(SimpleSearchFields.Color, ["W", "G", "R"], True)
    builder.add_field(SimpleSearchFields.Set, ["Set 1", "Set 2", "Set 3"], True)
    
    return make_response(jsonify({'query_string': builder.get_simple_search_query_string()}))


@app.route('/search/simple', methods=['GET'])
def search():
    name = request.args.get('name', '')
    builder = SimpleSearchBuilder()
    builder.add_field(SimpleSearchFields.Name, [name], False)
    query_string = builder.get_simple_search_query_string()
    search_result = parser_simple_search.parse_simple_search(query_string)
    return make_response(search_result)

@app.route('/card/<multiverse_id>')
def card_detail(multiverse_id):
    print multiverse_id
    return make_response(jsonify(parse_card_detail.parse_card_detail(multiverse_id)))


if __name__ == '__main__':
    app.debug = True
    app.run()
