__author__ = 'albertoguerreromartin'

# Regex patterns
SEARCH_MULTIVERSE_ID_PATTERN = '<span class="cardTitle">.*?multiverseid=(.*?)&.*?>'
SEARCH_TITLE_PATTERN = '<span class="cardTitle">.*?>(.*?) \('
SEARCH_MANA_COST_PATTERN = '<span class="manaCost">(.*?)</span>'
SEARCH_MANA_COST_SYMBOLS_PATTERN = 'name=(.*?)&'
SEARCH_EXPANSION_TEXT_PATTERN = '<div id=".*?_cardSetCurrent" class="rightCol">.*?<img title="(.*?)".*?</div>'
SEARCH_EXPANSION_IMAGE_URL_PATTERN = '<div id=".*?_cardSetCurrent".*?<img title=".*?" src="../../(.*?)".*?</div>'
SEARCH_TYPE_LINE_PATTERN = '<span class="typeLine">(.*?)</span>'
SEARCH_RULES_TEXT_PATTERN = '<div class="rulesText">(.*?)</div>'
SEARCH_RULES_IMAGES_URL_REPLACEMENT_PATTERN = 'src=\"/Handlers'

# Card detail patterns
CARD_NAME_PATTERN = 'id="ctl00_ctl00_ctl00_MainContent_SubContent_SubContent_nameRow".*?<div class="value">(.*?)</div>'
# TODO CARD_MANA_COST_PATTERN
CARD_CONVERTED_MANA_COST_PATTERN = 'id="ctl00_ctl00_ctl00_MainContent_SubContent_SubContent_cmcRow".*?<div class="value">(.*?)</div>'
CARD_TYPES_PATTERN = 'id="ctl00_ctl00_ctl00_MainContent_SubContent_SubContent_typeRow".*?<div class="value">(.*?)</div>'
CARD_TEXT_PATTERN = 'id="ctl00_ctl00_ctl00_MainContent_SubContent_SubContent_textRow".*?<div class="value">(.*?)</div>'
CARD_FLAVOR_TEXT_PATTERN = '<div class="flavortextbox">(.*?)</div>'
